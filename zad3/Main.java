package org.melszkowski.zut;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    //information signal properties:
    static double infSigFreq = 2.0;
    static double infSigAmplitude = 1.0;
    static double infsigSamplingFreq = 400.0;
    static double signalTime = 2.0;

    static Function zA1;
    static Function zA2;
    static Function zA3;

    static Function zP1;
    static Function zP2;
    static Function zP3;

    static List<Function> functionSet = new ArrayList<Function>();


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {

        Function infSig = generateInformationSignal(infSigFreq, infSigAmplitude, infsigSamplingFreq, signalTime);
        //drawFunction(infSig);

        zA1 = modulateAmplitude(infSig, 0.5);
        functionSet.add(zA1);
        //drawFunction(zA1);

        zA2 = modulateAmplitude(infSig, 8.0);
        functionSet.add(zA2);
        //drawFunction(zA2);

        zA3 = modulateAmplitude(infSig, 30.0);
        functionSet.add(zA3);
        //drawFunction(zA3);

        zP1 = modulatePhase(infSig, 0.5);
        functionSet.add(zP1);
        //drawFunction(zP1);

        zP2 = modulatePhase(infSig, 8.0);
        functionSet.add(zP2);
        //drawFunction(zP2);

        zP3 = modulatePhase(infSig, 40.0);
        functionSet.add(zP3);
        //drawFunction(zP3);

        for (Function fun : functionSet) {
           drawDiscreteFunction(discreteFourierTransform(fun));
        }

        drawDiscreteFunction(scaleToDecibelScale(discreteFourierTransform(zA1)));
        /*
        szerokosc pasmaodczytana z wykresu = 3.375 Hz
        f min = 13.5
        f max = 16.875
         */
    }

    public static Function scaleToDecibelScale(Function inputSpectrum) {
        Function output = new Function(inputSpectrum.id + " (in dB scale)","f","dB");
        output.xValues = new double[inputSpectrum.xValues.length];
        output.yValues = new double[inputSpectrum.yValues.length];

        for (int i = 0; i < output.xValues.length; i++) {
            output.xValues[i] = inputSpectrum.xValues[i];
            if(Math.abs(inputSpectrum.yValues[i]) > 0.1){
                output.yValues[i] = 20.0 * Math.log10(inputSpectrum.yValues[i]);
            } else {
                output.yValues[i] = 0.0;
            }
        }
        return output;
    }

    public static Function discreteFourierTransform(Function inputFun) {
        Function outputFun = new Function(inputFun.id + " - DFT", "f", "A");

        // N = number of samples of processed "signal"
        int N = inputFun.xValues.length;
        double sRe;
        double sIm;
        double arg;

        outputFun.xValues = new double[inputFun.xValues.length / 2];
        outputFun.yValues = new double[inputFun.yValues.length / 2];
        double fs = 300.0;

        // calculate half of N samples, as chart is symmetrical and only first half is drawn
        for (int k = 0; k < N / 2; k++) {
            sRe = 0.0;
            sIm = 0.0;

            for (int n = 0; n < N; n++) {
                arg = -2.0 * Math.PI * k * n / N;
                sRe += inputFun.yValues[n] * Math.cos(arg);
                sIm += inputFun.yValues[n] * Math.sin(arg);
            }
            outputFun.yValues[k] = Math.sqrt(Math.pow(sRe, 2) + Math.sqrt(Math.pow(sIm, 2)));
            outputFun.xValues[k] = k * fs / N;

        }
        return outputFun;
    }

    public static Function modulateAmplitude(Function inputFun, double kA) {

        Function modulated = new Function("sygnał z modulacją amplitudy: kA = " + kA);
        modulated.xValues = inputFun.xValues;
        modulated.yValues = new double[inputFun.yValues.length];

        for (int i = 0; i < inputFun.xValues.length; i++) {
            modulated.yValues[i] = (kA * inputFun.yValues[i] + 1.0) * Math.cos(2.0 * Math.PI * infSigFreq * 10.0 * i / infsigSamplingFreq);
        }
        return modulated;
    }

    public static Function modulatePhase(Function inputFun, double kP) {

        Function modulated = new Function("sygnał z modulacją kąta: kP = " + kP);
        modulated.xValues = inputFun.xValues;
        modulated.yValues = new double[inputFun.yValues.length];

        for (int i = 0; i < inputFun.xValues.length; i++) {
            modulated.yValues[i] = Math.cos((2.0 * Math.PI * infSigFreq * 10.0 * i / infsigSamplingFreq) + kP * inputFun.yValues[i]);
        }
        return modulated;
    }

    public static Function generateInformationSignal(double freq, double amplitude, double samplingFreq, double time) {
        Function out = new Function("sygnal informacyjny");

        int numberOfSamples = (int) (samplingFreq * time);
        out.xValues = new double[numberOfSamples];
        out.yValues = new double[numberOfSamples];

        double deltaTime = 1 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            out.xValues[i] = currentTime;
            out.yValues[i] = Math.sin(2 * Math.PI * freq * i / samplingFreq);
            currentTime += deltaTime;
        }

        return out;
    }

    private static void drawFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setCreateSymbols(false);
        lineChart.setTitle(function.id);
        LineChart.Series series = new LineChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new LineChart.Data(function.xValues[i], function.yValues[i]));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }

    private static void drawDiscreteFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final BarChart<String, Number> bc = new BarChart<>(xAxis, yAxis);
        bc.setLegendVisible(false);
        bc.setAnimated(false);
        bc.setBarGap(0);
        bc.setCategoryGap(0);
        bc.setVerticalGridLinesVisible(false);
        bc.setHorizontalGridLinesVisible(false);
        bc.setHorizontalZeroLineVisible(false);
        bc.setVerticalZeroLineVisible(false);
        bc.setMinSize(800, 600);

        XYChart.Series series = new XYChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new XYChart.Data(Double.toString(function.xValues[i]), function.yValues[i]));
        }

        Scene scene = new Scene(bc, 1024, 768);
        bc.getData().add(series);

        xAxis.setLabel(function.xLabel);
        yAxis.setLabel(function.yLabel);

        stage.setScene(scene);
        stage.show();
    }

}