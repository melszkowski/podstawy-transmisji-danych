package org.melszkowski.zut;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.regex.Pattern;

public class Main extends Application {

    static Function lowStateASK;
    static Function highStateASK;

    static Function lowStateFSK;
    static Function highStateFSK;

    static Function lowStatePSK;
    static Function highStatePSK;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {

        String binaryPattern = "10101100";
        //String binaryPattern = "1111111101010100100100101001001111010101";


        int N = 4;
        int bitsPerSecond = 2;
        //Tb - duration of one bit
        double Tb = 1.0 / bitsPerSecond;

        double fn = N * bitsPerSecond;
        lowStateASK = Function.generateSineWave(0.5, fn, 1024.0, Tb);
        highStateASK = Function.generateSineWave(1.0, fn, 1024.0, Tb);

        lowStateFSK = Function.generateSineWave(1.0, (N + 1) / Tb, 1024.0, Tb);
        highStateFSK = Function.generateSineWave(1.0, (N + 2) / Tb, 1024.0, Tb);

        lowStatePSK = Function.generateSineWave(1.0, fn, 1024.0, Tb, 0.0);
        highStatePSK = Function.generateSineWave(1.0, fn, 1024.0, Tb, Math.PI);

        Function signalASK = createKeyedWave(binaryPattern, lowStateASK, highStateASK);
        signalASK.id = "sygnal kluczowany ASK, ciag: " + binaryPattern;
        Function signalFSK = createKeyedWave(binaryPattern, lowStateFSK, highStateFSK);
        signalFSK.id = "sygnal kluczowany FSK, ciag: " + binaryPattern;
        Function signalPSK = createKeyedWave(binaryPattern, lowStatePSK, highStatePSK);
        signalPSK.id = "sygnal kluczowany PSK, ciag: " + binaryPattern;
        //drawFunction(signalASK);
        //drawFunction(signalFSK);
        //drawFunction(signalPSK);

        Function spectrumASK = Function.discreteFourierTransform(signalASK);
        Function spectrumFSK = Function.discreteFourierTransform(signalFSK);
        Function spectrumPSK = Function.discreteFourierTransform(signalPSK);

        drawDiscreteFunction(spectrumASK);
        drawDiscreteFunction(spectrumFSK);
        drawDiscreteFunction(spectrumPSK);

        //szerokosci pasma odczytane ręcznie z wykresu dla losowego, wejsciowego ciagu bitow o dlugosci 40
        drawDiscreteFunction(Function.scaleToDecibelScale(spectrumASK));
        System.out.println("Szerokość pasma ASK: ok. 4,2 Hz ");
        drawDiscreteFunction(Function.scaleToDecibelScale(spectrumFSK));
        System.out.println("Szerokość pasma FSK: ok. 8,2 Hz ");
        drawDiscreteFunction(Function.scaleToDecibelScale(spectrumPSK));
        System.out.println("Szerokość pasma PSK: ok. 4,2 Hz ");


    }

    private static Function createKeyedWave(String binarySignal, Function lowStateRepresentation, Function highStateRepresentation) {
        Function output = new Function();
        output.xLabel = "f";
        output.yLabel = "A";
        output.id = binarySignal;
        boolean isInputValid = Pattern.matches("[0-1]*", binarySignal);
        if (binarySignal.length() > 0 && isInputValid) {

            output.xValues = new double[0];
            output.yValues = new double[0];
            for (int i = 0; i < binarySignal.length(); i++) {
                if (binarySignal.charAt(i) == '0') {
                    output = Function.concatenateFunctions(output, lowStateRepresentation);
                } else {
                    output = Function.concatenateFunctions(output, highStateRepresentation);
                }
            }
        } else {
            System.exit(1);
        }

        return output;
    }

    private static void drawDiscreteFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final BarChart<String, Number> bc = new BarChart<>(xAxis, yAxis);
        bc.setLegendVisible(false);
        bc.setAnimated(false);
        bc.setBarGap(0);
        bc.setCategoryGap(0);
        bc.setVerticalGridLinesVisible(false);
        bc.setHorizontalGridLinesVisible(false);
        bc.setHorizontalZeroLineVisible(false);
        bc.setVerticalZeroLineVisible(false);
        bc.setMinSize(800, 600);

        XYChart.Series series = new XYChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new XYChart.Data(Double.toString(function.xValues[i]), function.yValues[i]));
        }

        Scene scene = new Scene(bc, 1024, 768);
        bc.getData().add(series);

        xAxis.setLabel(function.xLabel);
        yAxis.setLabel(function.yLabel);

        stage.setScene(scene);
        stage.show();
    }

    private static void drawFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setCreateSymbols(false);
        lineChart.setTitle(function.id);
        LineChart.Series series = new LineChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new LineChart.Data(function.xValues[i], function.yValues[i]));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }
}