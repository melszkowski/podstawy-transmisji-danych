package org.melszkowski.zut;

import java.util.Arrays;
import java.util.stream.Stream;

public class Function {

    double[] xValues;
    double[] yValues;
    String id;
    String xLabel;
    String yLabel;

    public Function(){
        this("","","");
    }

    public Function(String id) {
        this(id,"","");
    }

    public Function(String id, String xLabel, String yLabel) {
        this.id = id;
        this.xLabel = xLabel;
        this.yLabel = yLabel;
    }

    public static Function concatenateFunctions(Function first, Function second) {

        if(first.xValues.length == 0){
            second.id = "";
            return second;
        }

        Function result = new Function();
        int numOfSamples = first.xValues.length + second.xValues.length;
        result.xValues = new double[numOfSamples];
        result.yValues = new double[numOfSamples];

        for (int i = 0; i < first.xValues.length; i++) {
            result.xValues[i] = first.xValues[i];
            result.yValues[i] = first.yValues[i];
        }

        for (int i = 0; i < second.xValues.length; i++) {
            result.xValues[i + first.xValues.length] = second.xValues[i] + first.xValues[first.xValues.length-1];
            result.yValues[i + first.yValues.length] = second.yValues[i];
        }

        return result;
    }

    public static Function generateSineWave(double amplitude, double freq, double samplingFreq, double timeOfOneBit) {
        return Function.generateSineWave(amplitude,freq,samplingFreq,timeOfOneBit,0.0);
    }

    public static Function generateSineWave(double amplitude, double freq, double samplingFreq, double timeOfOneBit, double phase) {
        int numberOfSamples = (int) (samplingFreq * timeOfOneBit);

        Function out = new Function("A = " + amplitude + ", freq = " + freq + ", samplingFreq = " + samplingFreq);
        out.xValues = new double[numberOfSamples];
        out.yValues = new double[numberOfSamples];

        double deltaTime = 1.0 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            out.xValues[i] = currentTime;
            out.yValues[i] = amplitude * Math.sin(2 * Math.PI * freq * i / samplingFreq + phase);
            currentTime += deltaTime;
        }
        return out;
    }

    public static Function discreteFourierTransform(Function inputFun) {
        Function outputFun = new Function(inputFun.id + " - DFT", "f", "A");

        // N = number of samples of processed "signal"
        int N = inputFun.xValues.length;
        double sRe;
        double sIm;
        double arg;

        outputFun.xValues = new double[inputFun.xValues.length / 2];
        outputFun.yValues = new double[inputFun.yValues.length / 2];
        double fs = 300.0;

        // calculate half of N samples, as chart is symmetrical and only first half is drawn
        for (int k = 0; k < N / 2; k++) {
            sRe = 0.0;
            sIm = 0.0;

            for (int n = 0; n < N; n++) {
                arg = -2.0 * Math.PI * k * n / N;
                sRe += inputFun.yValues[n] * Math.cos(arg);
                sIm += inputFun.yValues[n] * Math.sin(arg);
            }
            outputFun.yValues[k] = Math.sqrt(Math.pow(sRe, 2) + Math.sqrt(Math.pow(sIm, 2)));
            outputFun.xValues[k] = k * fs / N;

        }
        return outputFun;
    }

    public static Function scaleToDecibelScale(Function inputSpectrum) {
        Function output = new Function(inputSpectrum.id + " (in dB scale)","f","dB");
        output.xValues = new double[inputSpectrum.xValues.length];
        output.yValues = new double[inputSpectrum.yValues.length];

        double val;
        for (int i = 0; i < output.xValues.length; i++) {
            output.xValues[i] = inputSpectrum.xValues[i];
            if(Math.abs(inputSpectrum.yValues[i]) > 0.01){
                val = 20.0 * Math.log10(inputSpectrum.yValues[i]);
                output.yValues[i] = val > 0 ? val : 0.0;
            } else {
                output.yValues[i] = 0.0;
            }
        }
        return output;
    }

}

