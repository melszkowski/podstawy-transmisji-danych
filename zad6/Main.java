import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.Random;
import java.util.regex.Pattern;

public class Main extends Application {

    static Function lowStateASK;
    static Function highStateASK;

    int bitsPerSecond;
    double Tb;
    double samplingFreq;
    double fn;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {
        // wiadomosc musi miec dlugosc bedaca wielokrotnoscia 4
        String message = "11110000101001010011";
        System.out.println("Wiadomosc: \t" + message);

        // KODER:
        String code = generate74HammingCode(message);
        System.out.println("Zakodowana wiadomosc: \t" + code);

        // MODULATOR (ASK):

        int N = 4;
        bitsPerSecond = 2;
        //Tb - duration of one bit
        Tb = 1.0 / bitsPerSecond;
        samplingFreq = 1024.0;
        fn = N * bitsPerSecond;
        lowStateASK = Function.generateSineWave(0.5, fn, samplingFreq, Tb);
        highStateASK = Function.generateSineWave(1.0, fn, samplingFreq, Tb);
        Function signalASK = createKeyedWave(code, lowStateASK, highStateASK);
        signalASK.id = "ASK signal, pattern: " + code;
        signalASK.setLabels("A", "f");
        drawFunction(signalASK);

        // KANAL TRANSMISYJNY (tu przykladowo skopiowanie sygnału na miejsce nowego):

        Function transmittedSignal = signalASK.clone();

        // DEMODULATOR:

        //mnozenie funkcji:
        Function XtASK = Function.convoluteFunctions(transmittedSignal, highStateASK);
        //calkowanie:
        Function PtASK = Function.integrateFunctionBitByBit(XtASK, samplingFreq, bitsPerSecond);
        //odzyskiwanie sygnalu z zadanym progiem:
        Function restoredBinarySignalFromASK = Function.getBinarySignalFromWave(PtASK, 130.0);
        //drawFunction(restoredBinarySignalFromASK);

        String codeReceived = getCodedMessageFromSignal(restoredBinarySignalFromASK);
        System.out.println("Zakodowana wiadomosc na podstawie przeslanego, binarnego sygnalu: \t" + codeReceived);


        // DEKODER:

        String solution = decode74HammingCode(codeReceived);
        System.out.println("Odkodowana wiadomosc: \t" + solution);

        String manipulatedSignal = code.substring(0, 2) + negateBitAsChar(code.charAt(2)) + code.substring(3, code.length());
        System.out.println("Celowo przeklamany 1 bit zakodowanej wiadomosci: \t" + manipulatedSignal);

        String manipulatedSignalSolution = decode74HammingCode(manipulatedSignal);
        System.out.println("Odkodowana przeklamana powyzsza wiadomosc: \t" + manipulatedSignalSolution + " - powodzenie");

        // WPROWADZENIE SZUMU:

        int numberOfSamples = transmittedSignal.xValues.length;
        Function noise = generateNoise(numberOfSamples);
        //no gain, alpha = 1:
        Function noisySignal = Function.addFunctions(transmittedSignal, noise);
        noisySignal.id = "Zaszumiony sygnal, alpha=1.0";
        drawFunction(noisySignal);

        // demodulacja zaszumionego sygnalu (1):
        System.out.println("\nSzum ma rozklad normalny:");
        System.out.println("srednia = 0.0");
        System.out.println("odch. standardowe = 0.2");

        XtASK = Function.convoluteFunctions(noisySignal, highStateASK);
        PtASK = Function.integrateFunctionBitByBit(XtASK, samplingFreq, bitsPerSecond);
        restoredBinarySignalFromASK = Function.getBinarySignalFromWave(PtASK, 130.0);
        codeReceived = getCodedMessageFromSignal(restoredBinarySignalFromASK);

        System.out.println("Zakodowana wiadomosc na podstawie zaszumionego sygnalu (gain=1): \t" + codeReceived);
        System.out.println("BER wynosi: " + calculateBER(code, codeReceived) + " %");

        // alpha = 2.0
        noise.gain(2.0);
        drawFunction(noise);
        noisySignal = Function.addFunctions(transmittedSignal, noise);
        noisySignal.id = "Zaszumiony sygnal, alpha=2.0";
        drawFunction(noisySignal);
        // demodulacja zaszumionego sygnalu (2):

        XtASK = Function.convoluteFunctions(noisySignal, highStateASK);
        PtASK = Function.integrateFunctionBitByBit(XtASK, samplingFreq, bitsPerSecond);
        restoredBinarySignalFromASK = Function.getBinarySignalFromWave(PtASK, 130.0);
        codeReceived = getCodedMessageFromSignal(restoredBinarySignalFromASK);

        System.out.println("Zakodowana wiadomosc na podstawie zaszumionego sygnalu (gain=2.0): \t" + codeReceived);
        System.out.println("BER wynosi: " + calculateBER(code, codeReceived) + " %");

        // alpha = 3.0
        // wzmocnienie juz wynosi 2, wiec by osiagnac 3 mnozymy przez 1.5
        noise.gain(1.5);
        noisySignal = Function.addFunctions(transmittedSignal, noise);
        noisySignal.id = "Zaszumiony sygnal, alpha=3.0";
        drawFunction(noisySignal);

        // demodulacja zaszumionego sygnalu (3):

        XtASK = Function.convoluteFunctions(noisySignal, highStateASK);
        PtASK = Function.integrateFunctionBitByBit(XtASK, samplingFreq, bitsPerSecond);
        restoredBinarySignalFromASK = Function.getBinarySignalFromWave(PtASK, 130.0);
        codeReceived = getCodedMessageFromSignal(restoredBinarySignalFromASK);

        System.out.println("Zakodowana wiadomosc na podstawie zaszumionego sygnalu (gain=3.0): \t" + codeReceived);
        System.out.println("BER wynosi: " + calculateBER(code, codeReceived) + " %");
    }

    private double calculateBER(String bits1, String bits2) {
        if (bits1.length() != bits2.length()) {
            System.out.println("Unable to calculate BER - vectors' lengths are not equal");
            System.exit(1);
        }
        int numOfFailedBits = 0;
        for (int i = 0; i < bits1.length(); i++) {
            if (bits1.charAt(i) != bits2.charAt(i)) {
                numOfFailedBits++;
            }
        }
        return Math.round((numOfFailedBits * 100.0)/ (double) bits1.length());
    }

    private Function generateNoise(int numberOfSamples) {
        //rozklad normalny, ze wzgledu na czeste wystepowanie w przyrodzie.
        //srednia:
        double mean = 0.0;
        //odchylenie standardowe:
        double variance = 0.2;

        Random randomGenerator = new Random();
        Function out = new Function();
        out.xValues = new double[numberOfSamples];
        out.yValues = new double[numberOfSamples];
        double val;
        for (int i = 0; i < numberOfSamples; i++) {
            out.xValues[i] = i;
                val = mean + randomGenerator.nextGaussian() * variance;

            out.yValues[i] = val;
        }
        return out;

    }

    private String getCodedMessageFromSignal(Function restoredBinarySignalFromASK) {
        StringBuilder sb = new StringBuilder();

        int numOfhighSamples = 0;
        int samplesPerBit = (int) (Tb * samplingFreq);
        int expectedNumberOfBits = restoredBinarySignalFromASK.xValues.length / samplesPerBit;
        for (int currentBit = 0; currentBit < expectedNumberOfBits; currentBit++) {
            for (int currentSample = currentBit * samplesPerBit; currentSample < (currentBit + 1) * samplesPerBit; currentSample++) {
                if (restoredBinarySignalFromASK.yValues[currentSample] > 0.9) {
                    numOfhighSamples++;
                }
            }
            //jezeli wiecej niz 0.4 probek z bitu wskazuje na '1' - bit jest jedynka, w przeciwnym wypadku zerem
            if (numOfhighSamples > (int) 0.4 * samplesPerBit) {
                sb.append('1');
            } else {
                sb.append('0');
            }
            numOfhighSamples = 0;
        }

        return sb.toString();
    }

    private static String generate74HammingCode(String input) {
        if ((input.length() % 4) != 0) {
            System.out.println("wiadomosc musi miec dlugosc bedaca wielokrotnoscia 4. wyjscie z programu.");
            System.exit(1);
        }
        int iterations = input.length() / 4;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < iterations; i++) {

            String inputSubstring = input.substring(i * 4, (i * 4 + 4));
            char x1, x2, x3, x4, x5, x6, x7;
            x7 = inputSubstring.charAt(0);
            x6 = inputSubstring.charAt(1);
            x5 = inputSubstring.charAt(2);
            x3 = inputSubstring.charAt(3);

            //x1 = x3 XOR x5 XOR x7 = (x3 XOR x5) XOR x7
            x1 = xorBitsAsChars(x3, x5);
            x1 = xorBitsAsChars(x1, x7);

            //x2 = x3 XOR x6 XOR x7
            x2 = xorBitsAsChars(x3, x6);
            x2 = xorBitsAsChars(x2, x7);

            //x4 = x5 XOR x6 XOR x7
            x4 = xorBitsAsChars(x5, x6);
            x4 = xorBitsAsChars(x4, x7);

            sb.append(x7).append(x6).append(x5).append(x4).append(x3).append(x2).append(x1);
        }


        return sb.toString();
    }

    private static String decode74HammingCode(String code) {
        StringBuilder sb = new StringBuilder();
        int iterations = code.length() / 7;

        for (int i = 0; i < iterations; i++) {
            String codeSubstring = code.substring(i * 7, (i * 7 + 7));
            // arr[0] - empty
            // arr[1] is x1
            // arr[2] is x2
            // etc.
            char[] arr = new char[8];
            arr[7] = codeSubstring.charAt(0);
            arr[6] = codeSubstring.charAt(1);
            arr[5] = codeSubstring.charAt(2);
            arr[4] = codeSubstring.charAt(3);
            arr[3] = codeSubstring.charAt(4);
            arr[2] = codeSubstring.charAt(5);
            arr[1] = codeSubstring.charAt(6);

            //x1 = x3 XOR x5 XOR x7 = (x3 XOR x5) XOR x7
            char x1prim = xorBitsAsChars(arr[3], arr[5]);
            x1prim = xorBitsAsChars(x1prim, arr[7]);

            //x2 = x3 XOR x6 XOR x7
            char x2prim = xorBitsAsChars(arr[3], arr[6]);
            x2prim = xorBitsAsChars(x2prim, arr[7]);

            //x4 = x5 XOR x6 XOR x7
            char x4prim = xorBitsAsChars(arr[5], arr[6]);
            x4prim = xorBitsAsChars(x4prim, arr[7]);

            int S = 0;
            if (xorBitsAsChars(arr[1], x1prim) == '1')
                S += 1;
            if (xorBitsAsChars(arr[2], x2prim) == '1')
                S += 2;
            if (xorBitsAsChars(arr[4], x4prim) == '1')
                S += 4;

            if (S != 0) {
                arr[S] = negateBitAsChar(arr[S]);
            }

            sb.append(arr[7]).append(arr[6]).append(arr[5]).append(arr[3]);
        }

        return sb.toString();


    }

    private static char negateBitAsChar(char b) {
        if (b == '0')
            return '1';
        else
            return '0';
    }

    private static char xorBitsAsChars(char b1, char b2) {
        if (b1 == b2)
            return '0';
        else
            return '1';
    }

    private static Function createKeyedWave(String binarySignal, Function lowStateRepresentation, Function highStateRepresentation) {
        Function output = new Function();
        output.xLabel = "f";
        output.yLabel = "A";
        output.id = binarySignal;
        boolean isInputValid = Pattern.matches("[0-1]*", binarySignal);
        if (binarySignal.length() > 0 && isInputValid) {

            output.xValues = new double[0];
            output.yValues = new double[0];
            for (int i = 0; i < binarySignal.length(); i++) {
                if (binarySignal.charAt(i) == '0') {
                    output = Function.concatenateFunctions(output, lowStateRepresentation);
                } else {
                    output = Function.concatenateFunctions(output, highStateRepresentation);
                }
            }
        } else {
            System.exit(1);
        }

        return output;
    }

    private static void drawFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setCreateSymbols(false);
        lineChart.setTitle(function.id);
        LineChart.Series series = new LineChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new LineChart.Data(function.xValues[i], function.yValues[i]));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }
}