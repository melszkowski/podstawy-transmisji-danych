package org.melszkowski.zut;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class Main extends Application {

    //zad1
    static Function xFun = new Function("zad 1: x(n)");
    //zad2
    static Function yFun = new Function("zad 2: y(n)");
    static Function zFun = new Function("zad 2: z(n)");
    static Function vFun = new Function("zad 2: v(n)");
    //zad3
    static Function uFun = new Function("zad 3: u(n)");
    //zad4
    static Function gFun = new Function("zad 4: g(t)");

    static List<Function> functionSet = new ArrayList<Function>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {
        zad1();
        zad2();
        zad3();
        zad4();

        for (Function fun : functionSet) {
            drawFunction(fun);
        }
    }

    public static void zad1() {
        double freq = 3.5;
        double fi = 5 * Math.PI;
        double samplingFreq = 300;
        double time = 1.6;
        int numberOfSamples = (int) (samplingFreq * time);

        xFun.xValues = new double[numberOfSamples];
        xFun.yValues = new double[numberOfSamples];

        double deltaTime = 1 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            xFun.xValues[i] = currentTime;
            xFun.yValues[i] = Math.sin(2 * Math.PI * freq * i / samplingFreq + fi);
            currentTime += deltaTime;
        }

        functionSet.add(xFun);
    }

    public static void zad2() {

        int numberOfSamples = xFun.xValues.length;
        double samplingFreq = 300.0;

        yFun.xValues = new double[numberOfSamples];
        yFun.yValues = new double[numberOfSamples];

        zFun.xValues = new double[numberOfSamples];
        zFun.yValues = new double[numberOfSamples];

        vFun.xValues = new double[numberOfSamples];
        vFun.yValues = new double[numberOfSamples];

        //values on X axis taken from previous task (zad1)
        yFun.xValues = xFun.xValues;
        zFun.xValues = xFun.xValues;
        vFun.xValues = xFun.xValues;

        for (int i = 0; i < numberOfSamples; i++) {
            yFun.yValues[i] = Math.pow(3 * i + 2.7, 0.9) * Math.sin(12.0 * Math.PI * i / samplingFreq);

            zFun.yValues[i] = yFun.yValues[i] - Math.abs(xFun.yValues[i]);
            vFun.yValues[i] = xFun.yValues[i] * (Math.abs(yFun.yValues[i]) + 1.6) * Math.exp(-0.3 * i);
        }
        functionSet.add(zFun);
        functionSet.add(vFun);
    }

    public static void zad3() {
        double samplingFreq = 6000;
        double time = 3.0;
        int numberOfSamples = (int) (samplingFreq * time);

        uFun.xValues = new double[numberOfSamples];
        uFun.yValues = new double[numberOfSamples];

        double deltaTime = 1.0 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            uFun.xValues[i] = currentTime;

            if (currentTime < 0.3) {
                uFun.yValues[i] = currentTime * Math.sin(34.0 * Math.PI * i / samplingFreq);
            } else if (currentTime < 0.8) {
                uFun.yValues[i] = Math.cos(12.0 * Math.PI * currentTime / samplingFreq) / (2.0 * Math.pow(currentTime, 3)) * Math.cos(22.0 * Math.PI * currentTime / samplingFreq);
            } else if (currentTime < 1.4) {
                uFun.yValues[i] = Math.sin(12.0 * Math.PI * i / samplingFreq) / Math.pow(currentTime, 2);
            } else {
                uFun.yValues[i] = Math.sin(20.0 * Math.PI * i / samplingFreq) - (Math.log(currentTime) / Math.log(2));
            }

            currentTime += deltaTime;
        }
        functionSet.add(uFun);

    }

    public static void zad4() {
        double time = 3.0;
        double samplingFreq = 9000.0;
        int numberOfSamples = (int) (samplingFreq * time);
        double deltaTime = 1.0 / samplingFreq;
        double currentTime;

        gFun.xValues = new double[numberOfSamples];
        gFun.yValues = new double[numberOfSamples];

        int H = 40;
        currentTime = 0.0;

        Function tempFun10 = new Function("zad 4: g(t)  H = 10");
        Function tempFun20 = new Function("zad 4: g(t)  H = 20");
        Function tempFun40 = new Function("zad 4: g(t)  H = 40");

        tempFun10.yValues = new double[numberOfSamples];
        tempFun20.yValues = new double[numberOfSamples];
        tempFun40.yValues = new double[numberOfSamples];

        //for every sample:
        for (int i = 0; i < numberOfSamples; i++) {
            gFun.xValues[i] = currentTime;

            double sum = 0.0;

            for (int n = 1; n <= H; n++) {
                sum += 1.0 / (double) n * Math.sin((double) n * Math.PI * samplingFreq * 0.2) * Math.sin((double) n * Math.PI * i / samplingFreq);

                if (n == 10) {
                    tempFun10.yValues[i] = 4.0 / Math.PI * sum;
                } else if (n == 20) {
                    tempFun20.yValues[i] = 4.0 / Math.PI * sum;
                } else if (n == 40) {
                    tempFun40.yValues[i] = 4.0 / Math.PI * sum;
                }
                currentTime += deltaTime;
            }
        }
        tempFun10.xValues = gFun.xValues;
        tempFun20.xValues = gFun.xValues;
        tempFun40.xValues = gFun.xValues;

        functionSet.add(tempFun10);
        functionSet.add(tempFun20);
        functionSet.add(tempFun40);

    }

    private static void drawFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setCreateSymbols(false);
        lineChart.setTitle(function.id);
        LineChart.Series series = new LineChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new LineChart.Data(function.xValues[i], function.yValues[i]));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }

}

