import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.regex.Pattern;

public class Main extends Application {

    static Function lowStateASK;
    static Function highStateASK;

    static Function lowStateFSK;
    static Function highStateFSK;

    static Function lowStatePSK;
    static Function highStatePSK;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {

        String binaryPattern = "10101100";

        int N = 4;
        int bitsPerSecond = 2;
        //Tb - duration of one bit
        double Tb = 1.0 / bitsPerSecond;
        double samplingFreq = 1024.0;

        double fn = N * bitsPerSecond;
        lowStateASK = Function.generateSineWave(0.5, fn, samplingFreq, Tb);
        highStateASK = Function.generateSineWave(1.0, fn, samplingFreq, Tb);

        lowStateFSK = Function.generateSineWave(1.0, ((N + 1) / Tb), samplingFreq, Tb);
        highStateFSK = Function.generateSineWave(1.0, ((N + 2) / Tb), samplingFreq, Tb);

        lowStatePSK = Function.generateSineWave(1.0, fn, samplingFreq, Tb, 0.0);
        highStatePSK = Function.generateSineWave(1.0, fn, samplingFreq, Tb, Math.PI);

        //---ASK---
        Function signalASK = createKeyedWave(binaryPattern, lowStateASK, highStateASK);
        signalASK.id = "ASK signal, pattern: " + binaryPattern;
        signalASK.setLabels("A","f");
        drawFunction(signalASK);
        //mnozenie funkcji:
        Function XtASK = Function.convoluteFunctions(signalASK,highStateASK);
        drawFunction(XtASK);
        //calkowanie:
        Function PtASK = Function.integrateFunctionBitByBit(XtASK,samplingFreq,bitsPerSecond);
        drawFunction(PtASK);
        //odzyskiwanie sygnalu z zadanym progiem:
        Function restoredBinarySignalFromASK = Function.getBinarySignalFromWave(PtASK, 130.0);
        drawFunction(restoredBinarySignalFromASK);

        //---PSK---
        Function signalPSK = createKeyedWave(binaryPattern, lowStatePSK, highStatePSK);
        signalPSK.id = "PSK signal, pattern:  " + binaryPattern;
        signalPSK.setLabels("A","f");

        drawFunction(signalPSK);
        Function XtPSK = Function.convoluteFunctions(signalPSK,highStatePSK);
        drawFunction(XtPSK);
        Function PtPSK = Function.integrateFunctionBitByBit(XtPSK,samplingFreq,bitsPerSecond);
        drawFunction(PtPSK);
        Function restoredBinarySignalFromPSK = Function.getBinarySignalFromWave(PtPSK,0.0);
        drawFunction(restoredBinarySignalFromPSK);

//        //---FSK---
        Function signalFSK = createKeyedWave(binaryPattern, lowStateFSK, highStateFSK);
        signalFSK.id = "FSK signal, pattern: " + binaryPattern;
        signalFSK.setLabels("A","f");
        drawFunction(signalFSK);
        Function X1tFSK = Function.convoluteFunctions(signalFSK,lowStateFSK);
        Function X2tFSK = Function.convoluteFunctions(signalFSK,highStateFSK);
        drawFunction(X1tFSK);
        drawFunction(X2tFSK);
        Function integratedX1tFSK = Function.integrateFunctionBitByBit(X1tFSK,samplingFreq,bitsPerSecond);
        Function integratedX2tFSK = Function.integrateFunctionBitByBit(X2tFSK,samplingFreq,bitsPerSecond);
        //drawFunction(integratedX1tFSK);
        //drawFunction(integratedX2tFSK);
        Function PtFSK = Function.addFunctions(Function.flipFunctionOverXAxis(integratedX1tFSK), integratedX2tFSK);
        drawFunction(PtFSK);
        Function restoredBinarySignalFromFSK = Function.getBinarySignalFromWave(PtFSK, 10.0);
        drawFunction(restoredBinarySignalFromFSK);
    }



    private static Function createKeyedWave(String binarySignal, Function lowStateRepresentation, Function highStateRepresentation) {
        Function output = new Function();
        output.xLabel = "f";
        output.yLabel = "A";
        output.id = binarySignal;
        boolean isInputValid = Pattern.matches("[0-1]*", binarySignal);
        if (binarySignal.length() > 0 && isInputValid) {

            output.xValues = new double[0];
            output.yValues = new double[0];
            for (int i = 0; i < binarySignal.length(); i++) {
                if (binarySignal.charAt(i) == '0') {
                    output = Function.concatenateFunctions(output, lowStateRepresentation);
                } else {
                    output = Function.concatenateFunctions(output, highStateRepresentation);
                }
            }
        } else {
            System.exit(1);
        }

        return output;
    }

    private static void drawDiscreteFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final BarChart<String, Number> bc = new BarChart<>(xAxis, yAxis);
        bc.setLegendVisible(false);
        bc.setAnimated(false);
        bc.setBarGap(0);
        bc.setCategoryGap(0);
        bc.setVerticalGridLinesVisible(false);
        bc.setHorizontalGridLinesVisible(false);
        bc.setHorizontalZeroLineVisible(false);
        bc.setVerticalZeroLineVisible(false);
        bc.setMinSize(800, 600);

        XYChart.Series series = new XYChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new XYChart.Data(Double.toString(function.xValues[i]), function.yValues[i]));
        }

        Scene scene = new Scene(bc, 1024, 768);
        bc.getData().add(series);

        xAxis.setLabel(function.xLabel);
        yAxis.setLabel(function.yLabel);

        stage.setScene(scene);
        stage.show();
    }

    private static void drawFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setCreateSymbols(false);
        lineChart.setTitle(function.id);
        LineChart.Series series = new LineChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new LineChart.Data(function.xValues[i], function.yValues[i]));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }
}