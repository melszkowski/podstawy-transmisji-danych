Tasks from "Fundamentals of data transmission".
Solved using Java language with charts drawn using JavaFX.

Assignments include: 
- sine function generation, 
- DFT calculation, 
- Amplitude/Phase modulation, 
- Binary data encoding/decoding using Amplitude/Phase/Frequency shift keying, 
- Adding noise to signal and examining Signal To Noise ratio