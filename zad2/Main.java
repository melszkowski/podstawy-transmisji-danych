package org.melszkowski.zut;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
    //lab1:
    //zad1
    static Function xFun = new Function("zad 1: x(n)");
    //zad2
    static Function yFun = new Function("zad 2: y(n)");
    static Function zFun = new Function("zad 2: z(n)");
    static Function vFun = new Function("zad 2: v(n)");
    //zad3
    static Function uFun = new Function("zad 3: u(n)");
    //zad4
    static Function gFun = new Function("zad 4: g(t)");

    //lab2:
    static Function lab2zad3Fun = new Function("Lab2, zad3: z(n)");

    //contains all generated signals from LAB1
    static List<Function> functionSet = new ArrayList<Function>();


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage s) {
        //drawing all charts may take ~2 minutes
//        lab1zad1();
//        lab1zad2();
//        lab1zad3();
//        lab1zad4();
        lab2zad3();

        for (Function fun : functionSet) {
            drawDiscreteFunction(discreteFourierTransform(fun));
        }

    }

    public static Function discreteFourierTransform(Function inputFun) {
        Function outputFun = new Function(inputFun.id + " - DFT", "f", "A");

        // N = number of samples of processed "signal"
        int N = inputFun.xValues.length;
        double sRe;
        double sIm;
        double arg;

        outputFun.xValues = new double[inputFun.xValues.length / 2];
        outputFun.yValues = new double[inputFun.yValues.length / 2];
        double fs = 300.0;

        // calculate half of N samples, as chart is symmetrical and only first half is drawn
        for (int k = 0; k < N / 2; k++) {
            sRe = 0.0;
            sIm = 0.0;

            for (int n = 0; n < N; n++) {
                arg = -2.0 * Math.PI * k * n / N;
                sRe += inputFun.yValues[n] * Math.cos(arg);
                sIm += inputFun.yValues[n] * Math.sin(arg);
            }
            outputFun.yValues[k] = Math.sqrt(Math.pow(sRe, 2) + Math.sqrt(Math.pow(sIm, 2)));
            outputFun.xValues[k] = k * fs / N;

        }
        return outputFun;
    }

    public static void lab1zad1() {
        double freq = 3.5;
        double fi = 5 * Math.PI;
        double samplingFreq = 300;
        double time = 1.6;
        int numberOfSamples = (int) (samplingFreq * time);

        xFun.xValues = new double[numberOfSamples];
        xFun.yValues = new double[numberOfSamples];

        double deltaTime = 1 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            xFun.xValues[i] = currentTime;
            xFun.yValues[i] = Math.sin(2 * Math.PI * freq * i / samplingFreq + fi);
            currentTime += deltaTime;
        }

        functionSet.add(xFun);
    }

    public static void lab1zad2() {

        int numberOfSamples = xFun.xValues.length;
        double samplingFreq = 300.0;

        yFun.xValues = new double[numberOfSamples];
        yFun.yValues = new double[numberOfSamples];

        zFun.xValues = new double[numberOfSamples];
        zFun.yValues = new double[numberOfSamples];

        vFun.xValues = new double[numberOfSamples];
        vFun.yValues = new double[numberOfSamples];

        //values on X axis taken from previous task (zad1)
        yFun.xValues = xFun.xValues;
        zFun.xValues = xFun.xValues;
        vFun.xValues = xFun.xValues;

        for (int i = 0; i < numberOfSamples; i++) {
            yFun.yValues[i] = Math.pow(3 * i + 2.7, 0.9) * Math.sin(12.0 * Math.PI * i / samplingFreq);

            zFun.yValues[i] = yFun.yValues[i] - Math.abs(xFun.yValues[i]);
            vFun.yValues[i] = xFun.yValues[i] * (Math.abs(yFun.yValues[i]) + 1.6) * Math.exp(-0.3 * i);
        }
        functionSet.add(zFun);
        functionSet.add(vFun);
    }

    public static void lab1zad3() {
        double samplingFreq = 6000;
        double time = 3.0;
        int numberOfSamples = (int) (samplingFreq * time);

        uFun.xValues = new double[numberOfSamples];
        uFun.yValues = new double[numberOfSamples];

        double deltaTime = 1.0 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            uFun.xValues[i] = currentTime;

            if (currentTime < 0.3) {
                uFun.yValues[i] = currentTime * Math.sin(34.0 * Math.PI * i / samplingFreq);
            } else if (currentTime < 0.8) {
                uFun.yValues[i] = Math.cos(12.0 * Math.PI * currentTime / samplingFreq) / (2.0 * Math.pow(currentTime, 3)) * Math.cos(22.0 * Math.PI * currentTime / samplingFreq);
            } else if (currentTime < 1.4) {
                uFun.yValues[i] = Math.sin(12.0 * Math.PI * i / samplingFreq) / Math.pow(currentTime, 2);
            } else {
                uFun.yValues[i] = Math.sin(20.0 * Math.PI * i / samplingFreq) - (Math.log(currentTime) / Math.log(2));
            }

            currentTime += deltaTime;
        }
        functionSet.add(uFun);

    }

    public static void lab1zad4() {
        double time = 3.0;
        double samplingFreq = 9000.0;
        int numberOfSamples = (int) (samplingFreq * time);
        double deltaTime = 1.0 / samplingFreq;
        double currentTime;

        gFun.xValues = new double[numberOfSamples];
        gFun.yValues = new double[numberOfSamples];

        int H = 40;
        currentTime = 0.0;

        Function tempFun10 = new Function("zad 4: g(t)  H = 10");
        Function tempFun20 = new Function("zad 4: g(t)  H = 20");
        Function tempFun40 = new Function("zad 4: g(t)  H = 40");

        tempFun10.yValues = new double[numberOfSamples];
        tempFun20.yValues = new double[numberOfSamples];
        tempFun40.yValues = new double[numberOfSamples];

        //for every sample:
        for (int i = 0; i < numberOfSamples; i++) {
            gFun.xValues[i] = currentTime;

            double sum = 0.0;

            for (int n = 1; n <= H; n++) {
                sum += 1.0 / (double) n * Math.sin((double) n * Math.PI * samplingFreq * 0.2) * Math.sin((double) n * Math.PI * i / samplingFreq);

                if (n == 10) {
                    tempFun10.yValues[i] = 4.0 / Math.PI * sum;
                } else if (n == 20) {
                    tempFun20.yValues[i] = 4.0 / Math.PI * sum;
                } else if (n == 40) {
                    tempFun40.yValues[i] = 4.0 / Math.PI * sum;
                }
                currentTime += deltaTime;
            }
        }
        tempFun10.xValues = gFun.xValues;
        tempFun20.xValues = gFun.xValues;
        tempFun40.xValues = gFun.xValues;

        functionSet.add(tempFun10);
        functionSet.add(tempFun20);
        functionSet.add(tempFun40);

    }

    public static void lab2zad3() {
        double samplingFreq = 1024;
        double time = 1.0;
        int numberOfSamples = (int) (samplingFreq * time);

        lab2zad3Fun.xValues = new double[numberOfSamples];
        lab2zad3Fun.yValues = new double[numberOfSamples];

        double deltaTime = 1 / samplingFreq;
        double currentTime = 0.0;

        for (int i = 0; i < numberOfSamples; i++) {
            lab2zad3Fun.xValues[i] = currentTime;
            lab2zad3Fun.yValues[i] = Math.sin(200.0 * Math.PI * i / samplingFreq) + 0.5 * Math.sin(350.0 * Math.PI * i / samplingFreq);
            currentTime += deltaTime;
        }

        Fft fft = new Fft();

        fft.transform(lab2zad3Fun.xValues, lab2zad3Fun.yValues);
        Function lab2zad3FunFFT = new Function("lab 2, zad 3: z(n) - FFT", "f", "A");

        int numberOfFftSamples = numberOfSamples / 2;
        lab2zad3FunFFT.xValues = new double[numberOfFftSamples];
        lab2zad3FunFFT.yValues = new double[numberOfFftSamples];

        for (int i = 0; i < numberOfFftSamples; i++) {
            lab2zad3FunFFT.xValues[i] = i * samplingFreq / numberOfSamples;
            lab2zad3FunFFT.yValues[i] = Math.sqrt(Math.pow(lab2zad3Fun.xValues[i], 2) + Math.pow(lab2zad3Fun.yValues[i], 2));
        }
        drawDiscreteFunction(lab2zad3FunFFT);


    }

    private static void drawDiscreteFunction(Function function) {
        Stage stage = new Stage();
        stage.setTitle(function.id);

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final BarChart<String, Number> bc = new BarChart<>(xAxis, yAxis);
        bc.setLegendVisible(false);
        bc.setAnimated(false);
        bc.setBarGap(0);
        bc.setCategoryGap(0);
        bc.setVerticalGridLinesVisible(false);
        bc.setHorizontalGridLinesVisible(false);
        bc.setHorizontalZeroLineVisible(false);
        bc.setVerticalZeroLineVisible(false);
        bc.setMinSize(800, 600);

        XYChart.Series series = new XYChart.Series();
        for (int i = 0; i < function.xValues.length; i++) {
            series.getData().add(new XYChart.Data(Double.toString(function.xValues[i]), function.yValues[i]));
        }

        Scene scene = new Scene(bc, 1024, 768);
        bc.getData().add(series);

        xAxis.setLabel(function.xLabel);
        yAxis.setLabel(function.yLabel);

        stage.setScene(scene);
        stage.show();
    }

}