package org.melszkowski.zut;

public class Function {

    double[] xValues;
    double[] yValues;
    String id;
    String xLabel;
    String yLabel;

    public Function (String id){
        this.id = id;
        xLabel = "";
        yLabel = "";
    }

    public Function (String id, String xLabel, String yLabel){
        this.id = id;
        this.xLabel = xLabel;
        this.yLabel = yLabel;
    }
}
